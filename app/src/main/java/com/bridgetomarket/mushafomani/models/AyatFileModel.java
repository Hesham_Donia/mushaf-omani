package com.bridgetomarket.mushafomani.models;

import com.bridgetomarket.mushafomani.entities.Entity;
import com.bridgetomarket.mushafomani.entities.Filter;
import com.bridgetomarket.mushafomani.interfaces.FileListener;
import com.bridgetomarket.mushafomani.interfaces.MainListener;
import com.bridgetomarket.mushafomani.mappers.file_mappers.AyatFileMapper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by bridgetomarket on 8/1/16.
 */
public class AyatFileModel extends AbstractModel implements FileListener {

    /**
     * @param mapper
     */
    @Inject
    public AyatFileModel(AyatFileMapper mapper) {
        super(mapper);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void getDataFromFile(String fileName, MainListener<ArrayList<? extends Entity>> listener) {
        ((AyatFileMapper) mapper).getDataFromFile(fileName, listener);
    }
}
