package com.bridgetomarket.mushafomani.models;


import com.bridgetomarket.mushafomani.interfaces.ServiceListener;
import com.bridgetomarket.mushafomani.mappers.AbstractMapper;

/**
 * Created by hesham on 04/04/16.
 */
abstract public class AbstractModel implements ServiceListener {

    protected AbstractMapper mapper;

    /**
     * @param mapper
     */
    public AbstractModel(AbstractMapper mapper){
        this.mapper = mapper;
    }
}
