package com.bridgetomarket.mushafomani.interfaces;



import com.bridgetomarket.mushafomani.entities.Entity;
import com.bridgetomarket.mushafomani.entities.Filter;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
public interface ServiceListener {
    void find(String id, MainListener<ArrayList<? extends Entity>> listener);

    void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener);

    void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener);

    void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener);
}
