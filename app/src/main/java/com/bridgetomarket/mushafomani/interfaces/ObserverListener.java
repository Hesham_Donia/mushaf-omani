package com.bridgetomarket.mushafomani.interfaces;

/**
 * Created by hesham on 04/04/16.
 */
public interface ObserverListener {
    void update(boolean internetConnectionExist);
}
