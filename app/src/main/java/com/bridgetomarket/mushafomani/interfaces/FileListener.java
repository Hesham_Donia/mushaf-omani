package com.bridgetomarket.mushafomani.interfaces;

import com.bridgetomarket.mushafomani.entities.Entity;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 8/1/16.
 */
public interface FileListener {
    void getDataFromFile(String fileName, MainListener<ArrayList<? extends Entity>> listener);
}
