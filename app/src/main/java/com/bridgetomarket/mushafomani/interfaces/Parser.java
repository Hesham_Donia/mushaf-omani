package com.bridgetomarket.mushafomani.interfaces;


import com.bridgetomarket.mushafomani.entities.Entity;

/**
 * Created by heshamkhaled on 2/6/16.
 */
public interface Parser<T> {

    Entity parse(T object);

}
