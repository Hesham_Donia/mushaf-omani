package com.bridgetomarket.mushafomani.dagger;


import android.content.Context;

import com.bridgetomarket.mushafomani.MushafOmaniApplication;
import com.bridgetomarket.mushafomani.controllers.AyatController;


import javax.inject.Singleton;

import dagger.Module;
import dagger.ObjectGraph;
import dagger.Provides;

/**
 * Created by bridgetomarket on 8/1/16.
 */


@Module(
        injects = {MushafOmaniApplication.class, AyatController.class},

        includes = {AyatFileModule.class}
)

public class ApplicationModule {

    MushafOmaniApplication mApplication;

    public ApplicationModule(MushafOmaniApplication app) {
        mApplication = app;
    }

    @Provides
    @Singleton
    @ForApplication
    public Context provideApplicationContext() {
        return mApplication.getApplicationContext();
    }

    public ObjectGraph getObjectGraph() {
        return MushafOmaniApplication.objectGraph;
    }
}
