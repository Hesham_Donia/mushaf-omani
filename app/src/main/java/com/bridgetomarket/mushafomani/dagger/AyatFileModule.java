package com.bridgetomarket.mushafomani.dagger;

import android.content.Context;

import com.bridgetomarket.mushafomani.MushafOmaniApplication;
import com.bridgetomarket.mushafomani.mappers.file_mappers.AyatFileMapper;
import com.bridgetomarket.mushafomani.models.AyatFileModel;
import com.bridgetomarket.mushafomani.services.AyatFileService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by bridgetomarket on 8/1/16.
 */

@Module(
        library = true
        , complete = false, injects = {AyatFileService.class, AyatFileModel.class,
        AyatFileMapper.class}
)

public class AyatFileModule {
    @Provides
    AyatFileService provideAyatFileService() {
        return new AyatFileService(MushafOmaniApplication.objectGraph.get(AyatFileModel.class));
    }

    @Provides
    AyatFileModel provideAyatFileModel() {
        return new AyatFileModel(MushafOmaniApplication.objectGraph.get(AyatFileMapper.class));
    }

    @Provides
    AyatFileMapper provideAyatFileMapper(@ForApplication Context context) {
        return new AyatFileMapper(context);
    }
}
