package com.bridgetomarket.mushafomani.dagger;

import javax.inject.Qualifier;


@Qualifier
public @interface ForApplication {
}
