package com.bridgetomarket.mushafomani.controllers;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.bridgetomarket.mushafomani.MushafOmaniApplication;
import com.bridgetomarket.mushafomani.database.Aya;
import com.bridgetomarket.mushafomani.database.Sura;
import com.bridgetomarket.mushafomani.entities.Entity;
import com.bridgetomarket.mushafomani.interfaces.MainListener;
import com.bridgetomarket.mushafomani.services.AyatFileService;
import com.bridgetomarket.mushafomani.utils.CommonConstants;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 8/1/16.
 */
public class AyatController extends AbstractController {


    public final static int AYAT_FILE_ACTION = 1;

    public AyatController(Context context) {
        super(context);
    }

    @Override
    void requestHandler(int action, Bundle args) {
        setAction(action);
        mRequest = args;
        handleRequest();
    }

    private void handleRequest() {
        switch (mAction) {
            case AYAT_FILE_ACTION:
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "I'm here");
                getAyatDataFromFile();
                break;

            default:
                break;
        }
    }

    private void getAyatDataFromFile() {
        AyatFileService ayatFileService = MushafOmaniApplication.objectGraph.get(AyatFileService.class);
        ayatFileService.getDataFromFile(CommonConstants.JSON_FILE_NAME, new MainListener<ArrayList<? extends Entity>>() {
            @Override
            public void onSuccess(ArrayList<? extends Entity> result) {

                if (result != null) {
                    ArrayList<Aya> ayat = new ArrayList<>();
                    ArrayList<Sura> suraArrayList = (ArrayList<Sura>) result;
                    for (Sura sura : suraArrayList) {
                        for (Aya aya : sura.ayat) {
                            aya.setSura(sura);
                            ayat.add(aya);
                        }
                    }
                    addSuraToDatabase(suraArrayList);
                    addAyatToDatabase(ayat);
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void addAyatToDatabase(ArrayList<Aya> ayat) {
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "ayat length: " + ayat.size());
    }

    private void addSuraToDatabase(ArrayList<Sura> suraArrayList) {
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "sura length: " + suraArrayList.size());
    }
}
