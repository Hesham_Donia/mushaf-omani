package com.bridgetomarket.mushafomani.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by bridgetomarket on 8/1/16.
 */
public class DBAdapter extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "mushaf";
    private static final int DATABASE_VERSION = 1;
    private Dao<Sura, Integer> mSuraDao = null;
    private Dao<Aya, Integer> mAyaDao = null;
    private Dao<Reader, Integer> mReaderDao = null;
    private Dao<AyaReader, Integer> mAyaReaderDao = null;

    public DBAdapter(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Sura.class);
            TableUtils.createTable(connectionSource, Aya.class);
            TableUtils.createTable(connectionSource, Reader.class);
            TableUtils.createTable(connectionSource, AyaReader.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void close() {
        super.close();
        setSuraDao(null);
        setAyaDao(null);
        setReaderDao(null);
        setAyaReaderDao(null);
    }

    public Dao<Sura, Integer> getSuraDao() {
        if (mSuraDao == null) {
            try {
                mSuraDao = getDao(Sura.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mSuraDao;
    }

    public void setSuraDao(Dao<Sura, Integer> mSuraDao) {
        this.mSuraDao = mSuraDao;
    }

    public Dao<Aya, Integer> getAyaDao() {
        if (mAyaDao == null) {
            try {
                mAyaDao = getDao(Aya.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mAyaDao;
    }

    public void setAyaDao(Dao<Aya, Integer> mAyaDao) {
        this.mAyaDao = mAyaDao;
    }

    public Dao<Reader, Integer> getReaderDao() {
        if (mReaderDao == null) {
            try {
                mReaderDao = getDao(Reader.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mReaderDao;
    }

    public void setReaderDao(Dao<Reader, Integer> mReaderDao) {
        this.mReaderDao = mReaderDao;
    }

    public Dao<AyaReader, Integer> getAyaReaderDao() {
        if (mAyaReaderDao == null) {
            try {
                mAyaReaderDao = getDao(AyaReader.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return mAyaReaderDao;
    }

    public void setAyaReaderDao(Dao<AyaReader, Integer> mAyaReaderDao) {
        this.mAyaReaderDao = mAyaReaderDao;
    }
}
