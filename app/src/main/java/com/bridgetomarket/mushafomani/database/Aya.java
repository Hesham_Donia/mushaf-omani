package com.bridgetomarket.mushafomani.database;

import com.bridgetomarket.mushafomani.entities.Entity;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by bridgetomarket on 8/1/16.
 */

@DatabaseTable(tableName = "Aya")
public class Aya extends Entity {

    public static final String Aya_ID = "ID";
    public static final String Aya_Index = "Index";
    public static final String Aya_Text = "Text";
    public static final String Aya_PageNumber = "PageNumber";
    public static final String Aya_Sura = "Sura";

    public Aya() {

    }

    public Aya(Sura sura, int index, String text, int pageNumber) {
        setSura(sura);
        setIndex(index);
        setText(text);
        setPageNumber(pageNumber);
    }

    @DatabaseField(generatedId = true, columnName = Aya_ID)
    private int mId;

    @DatabaseField(columnName = Aya_Index)
    private int mIndex;

    @DatabaseField(columnName = Aya_PageNumber)
    private int mPageNumber;

    @DatabaseField(columnName = Aya_Text)
    private String mText;

    @DatabaseField(foreign = true, columnName = Aya_Sura)
    private Sura mSura;


    public int getId() {
        return mId;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int mIndex) {
        this.mIndex = mIndex;
    }

    public String getText() {
        return mText;
    }

    public void setText(String mText) {
        this.mText = mText;
    }

    public Sura getSura() {
        return mSura;
    }

    public void setSura(Sura mSura) {
        this.mSura = mSura;
    }

    public int getPageNumber() {
        return mPageNumber;
    }

    public void setPageNumber(int mPageNumber) {
        this.mPageNumber = mPageNumber;
    }
}
