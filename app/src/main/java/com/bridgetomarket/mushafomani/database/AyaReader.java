package com.bridgetomarket.mushafomani.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by bridgetomarket on 8/1/16.
 */
@DatabaseTable(tableName = "AyaReader")
public class AyaReader {

    public static final String AyaReader_ID = "ID";
    public static final String AyaReader_Aya = "Aya";
    public static final String AyaReader_Reader = "Reader";
    public static final String AyaReader_LocalPath = "LocalPath";

    @DatabaseField(generatedId = true, columnName = AyaReader_ID)
    private int mId;

    @DatabaseField(foreign = true, columnName = AyaReader_Aya)
    private Aya mAya;

    @DatabaseField(foreign = true, columnName = AyaReader_Reader)
    private Reader mReader;

    @DatabaseField(columnName = AyaReader_LocalPath)
    private String mLocalPath;


    public AyaReader() {

    }

    public AyaReader(Aya aya, Reader reader, String localPath) {
        setAya(aya);
        setReader(reader);
        setLocalPath(localPath);
    }

    public int getId() {
        return mId;
    }

    public Aya getAya() {
        return mAya;
    }

    public void setAya(Aya mAya) {
        this.mAya = mAya;
    }

    public Reader getReader() {
        return mReader;
    }

    public void setReader(Reader mReader) {
        this.mReader = mReader;
    }

    public String getLocalPath() {
        return mLocalPath;
    }

    public void setLocalPath(String mLocalPath) {
        this.mLocalPath = mLocalPath;
    }
}
