package com.bridgetomarket.mushafomani.database;

import com.bridgetomarket.mushafomani.entities.*;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 8/1/16.
 */

@DatabaseTable(tableName = "Sura")
public class Sura extends Entity {

    public static final String Sura_ID = "ID";
    public static final String Sura_Index = "Index";
    public static final String Sura_Name = "Name";

    @DatabaseField(generatedId = true, columnName = Sura_ID)
    private int mId;

    @DatabaseField(columnName = Sura_Index)
    private int mIndex;

    @DatabaseField(columnName = Sura_Name)
    private String mName;

    public ArrayList<Aya> ayat = null;

    public Sura() {
        ayat = new ArrayList<>();
    }

    public Sura(int index, String name) {
        setIndex(index);
        setName(name);
        ayat = new ArrayList<>();
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int mIndex) {
        this.mIndex = mIndex;
    }
}
