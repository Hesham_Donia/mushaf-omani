package com.bridgetomarket.mushafomani.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by bridgetomarket on 8/1/16.
 */
@DatabaseTable(tableName = "Reader")
public class Reader {
    public static final String Reader_ID = "ID";
    public static final String Reader_Name = "Name";

    @DatabaseField(generatedId = true, columnName = Reader_ID)
    private int mId;

    @DatabaseField(columnName = Reader_Name)
    private String mName;

    public Reader() {
    }

    public Reader(String name) {
        setName(name);
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }
}
