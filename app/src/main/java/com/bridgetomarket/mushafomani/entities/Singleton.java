package com.bridgetomarket.mushafomani.entities;

import com.bridgetomarket.mushafomani.interfaces.ObserverListener;
import com.bridgetomarket.mushafomani.views.activities.AbstractActivity;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by heshamkhaled on 2/7/16.
 */
public class Singleton {

    public static Stack<AbstractActivity> activityStack = new Stack<>();
    public static AbstractActivity currentActivity;
    public ArrayList<ObserverListener> observers = new ArrayList<>();
    private static Singleton singleton = null;

    public static Singleton getInstance() {
        if (singleton == null) {
            singleton = new Singleton();
        }

        return singleton;
    }

}
