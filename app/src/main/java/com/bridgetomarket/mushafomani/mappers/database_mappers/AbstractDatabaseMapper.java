package com.bridgetomarket.mushafomani.mappers.database_mappers;

import android.content.Context;


import com.bridgetomarket.mushafomani.database.DBAdapter;
import com.bridgetomarket.mushafomani.entities.Entity;
import com.bridgetomarket.mushafomani.interfaces.DatabaseListener;
import com.bridgetomarket.mushafomani.interfaces.MainListener;
import com.bridgetomarket.mushafomani.mappers.AbstractMapper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.StatementBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Created by hesham on 04/04/16.
 */
abstract public class AbstractDatabaseMapper extends AbstractMapper implements DatabaseListener {

    protected Context mContext;
    protected DBAdapter mDbAdapter;
    protected Dao mDao;

    public AbstractDatabaseMapper(Context context) {
        mContext = context;
        mDbAdapter = new DBAdapter(context);
    }

    public void executeOperation(DatabaseOperation operation, MainListener listener, Dao dao, StatementBuilder statementBuilder, Entity value) {
        switch (operation) {
            case SELECT:
                executeSelect(dao, statementBuilder, listener);
                break;

            case INSERT:
                executeInsert(dao, listener, value);
                break;

            case UPDATE:
                executeUpdate(statementBuilder, listener);
                break;

            case DELETE:
                executeDelete(statementBuilder, listener);
                break;
        }
    }

    private void executeSelect(Dao dao, StatementBuilder statementBuilder, MainListener listener) {
        if (statementBuilder == null) {
            try {
                listener.onSuccess(dao.queryForAll());
            } catch (SQLException e) {
                e.printStackTrace();
                listener.onError(e.toString());
            }
        } else {
            QueryBuilder queryBuilder = (QueryBuilder) statementBuilder;
            try {

                ArrayList<Entity> result = (ArrayList<Entity>) queryBuilder.query();

                if (result.size() > 0) {
                    listener.onSuccess(result);
                } else {
                    listener.onSuccess(null);
                }

            } catch (SQLException e) {
                e.printStackTrace();
                listener.onError(e.toString());
            }
        }
    }

    private void executeInsert(Dao dao, MainListener listener, Entity value) {
        try {
            dao.create(value);
            listener.onSuccess(true);
        } catch (SQLException e) {
            e.printStackTrace();
            listener.onError(e.toString());
        }
    }

    private void executeUpdate(StatementBuilder statementBuilder, MainListener listener) {
        UpdateBuilder updateBuilder = (UpdateBuilder) statementBuilder;
        try {
            updateBuilder.update();
            listener.onSuccess(true);
        } catch (SQLException e) {
            e.printStackTrace();
            listener.onError(e.toString());
        }
    }

    private void executeDelete(StatementBuilder statementBuilder, MainListener listener) {
        DeleteBuilder deleteBuilder = (DeleteBuilder) statementBuilder;

        try {
            deleteBuilder.delete();
            listener.onSuccess(true);
        } catch (SQLException e) {
            e.printStackTrace();

        }

    }

    protected void makeTransaction(final ArrayList<? extends Entity> value, final int from, final int to, final MainListener<Boolean> listener) {
        try {
            TransactionManager.callInTransaction(mDao.getConnectionSource(),
                    new Callable<Void>() {
                        public Void call() throws Exception {
                            // Your code from above
                            for (int i = from; i < to; i++) {
                                executeOperation(DatabaseOperation.INSERT, listener, mDao, mDao.queryBuilder(), value.get(i));
                            }
                            return null;
                        }
                    });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected enum DatabaseOperation {SELECT, INSERT, UPDATE, DELETE}

}
