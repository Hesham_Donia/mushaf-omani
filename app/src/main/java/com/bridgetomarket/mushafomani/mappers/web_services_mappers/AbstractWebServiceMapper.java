package com.bridgetomarket.mushafomani.mappers.web_services_mappers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bridgetomarket.mushafomani.entities.Entity;
import com.bridgetomarket.mushafomani.interfaces.MainListener;
import com.bridgetomarket.mushafomani.interfaces.Parser;
import com.bridgetomarket.mushafomani.interfaces.ServiceListener;
import com.bridgetomarket.mushafomani.mappers.AbstractMapper;
import com.bridgetomarket.mushafomani.mappers.VolleyQueue.QueueWrapper;
import com.bridgetomarket.mushafomani.utils.CommonConstants;
import com.bridgetomarket.mushafomani.utils.Language;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


abstract public class AbstractWebServiceMapper extends AbstractMapper implements ServiceListener {

    public String requestTag;
    public int statusCode;
    protected Context mContext;
    protected QueueWrapper mQueueWrapper;
    protected Parser parser = null;
    protected String mLanguage;
    protected String mFullUrl;

    public AbstractWebServiceMapper(Context ctx) {
        mContext = ctx;
        mQueueWrapper = new QueueWrapper(mContext);
        mLanguage = Language.getCurrentLang(mContext);
    }

    public String getRequestTag() {
        return requestTag;
    }

    /**
     * returns the url needed to do the http request to the service
     *
     * @return String
     */
    abstract protected String getUrl();

    /**
     * @return tag of the request
     */
    protected abstract String initRequestTag();

    public void executeRequest(JsonRequestType requestType, int method, MainListener listener) {
        initParser();
        switch (requestType) {
            case JSON_ARRAY:
                executeJsonArrayRequest(method, listener);
                break;
            case JSON_OBJECT:
                executeJSonObjectRequest(method, listener);
                break;
        }
    }

    protected String getFullUrl() {
        return CommonConstants.BASIC_URL + getUrl();
    }

//    private JSONObject getNeededParams(Entity entity) {
//        JSONObject paramJsonObject = new JSONObject();
//        HashMap<String, String> params = new HashMap<>();
//        if (entity instanceof User) {
//            User user = (User) entity;
//            params.put("data", user.userDataAsJson);
//            Log.d(CommonConstants.APPLICATION_LOG_TAG, "user.userDataAsJson: " + user.userDataAsJson);
//            paramJsonObject = new JSONObject(params);
//
//        } else if (entity instanceof Filter) {
//            Filter filter = (Filter) entity;
//            JSONObject mainJsonObject = new JSONObject();
//            JSONArray dataJsonArray = new JSONArray();
//            JSONObject detailsJsonObject = new JSONObject();
//            try {
//                if (filter.searchQuery != null) {
//                    detailsJsonObject.put("keyword", filter.searchQuery);
//                }
//
//                if (filter.sortType != null) {
//                    detailsJsonObject.put("SortType", filter.sortType);
//                }
//
//                if (filter.categoryId != 0) {
//                    detailsJsonObject.put("cat_id", filter.categoryId);
//                }
//
//                if (filter.tags != null) {
//                    detailsJsonObject.put("tags", filter.tags);
//                }
//
//                if (filter.id != 0) {
//                    detailsJsonObject.put("user_id", filter.id);
//                }
//
//                if (filter.productId != 0) {
//                    detailsJsonObject.put("product_id", filter.productId);
//                }
//
//                if (filter.productsIds != null) {
//                    detailsJsonObject.put("product_id", filter.productsIds);
//                }
//
//                if (filter.totalPrice != 0.0) {
//                    detailsJsonObject.put("total", filter.totalPrice);
//                }
//
//                if (filter.countryId != 0) {
//                    detailsJsonObject.put("country_id", filter.countryId);
//                }
//
//                if (filter.priceFrom != mContext.getResources().getInteger(R.integer.startPriceRange)
//                        || filter.priceTo != mContext.getResources().getInteger(R.integer.endPriceRange)) {
//                    detailsJsonObject.put("Pricefrom", filter.priceFrom);
//                    detailsJsonObject.put("priceto", filter.priceTo);
//                }
//
//                if (filter.email != null) {
//                    detailsJsonObject.put("email", filter.email);
//                }
//
//                if (filter.password != null) {
//                    detailsJsonObject.put("oldpass", filter.password);
//                }
//
//                if (filter.password != null) {
//                    detailsJsonObject.put("newpass", filter.newPassword);
//                }
//
//                if (filter.remainderId != 0) {
//                    detailsJsonObject.put("id", filter.remainderId);
//                }
//
//                if (filter.orderId != null) {
//                    detailsJsonObject.put("order_id", filter.orderId);
//                }
//
//                if (filter.countryName != null) {
//                    detailsJsonObject.put("country_name", filter.countryName);
//                }
//
//                if (filter.rate != 0) {
//                    detailsJsonObject.put("rate", filter.rate);
//                }
//
//                if (filter.promotionCode != null) {
//                    detailsJsonObject.put("promotion_codes", filter.promotionCode);
//                }
//
//                dataJsonArray.put(detailsJsonObject);
//
//                mainJsonObject.put("data", dataJsonArray);
//                paramJsonObject.put("data", mainJsonObject.toString());
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        } else if (entity instanceof Order) {
//            Order order = (Order) entity;
//            JSONObject mainJsonObject = new JSONObject();
//            JSONArray dataJsonArray = new JSONArray();
//            JSONObject detailsJsonObject = new JSONObject();
//            JSONArray productsJsonArray = new JSONArray();
//            try {
//                detailsJsonObject.put("user_id", Singleton.getInstance().currentUser.id);
//                detailsJsonObject.put("total_price", String.valueOf(order.finalPrice));
//                detailsJsonObject.put("original_total_price", String.valueOf(order.originalPrice));
//                detailsJsonObject.put("points", order.pointsPrice);
//                detailsJsonObject.put("bill_address_X", order.billingPlace.latitude);
//                detailsJsonObject.put("bill_address_Y", order.billingPlace.longitude);
//                detailsJsonObject.put("bill_address", order.billingPlace.address);
//                detailsJsonObject.put("deliver_address_X", order.deliverPlace.latitude);
//                detailsJsonObject.put("deliver_address_Y", order.deliverPlace.longitude);
//                detailsJsonObject.put("deliver_address", order.deliverPlace.address);
//                detailsJsonObject.put("deliver_address_phone", order.deliverPlace.phone);
//                detailsJsonObject.put("shipping_date", order.shippingDate);
//                if (order.subPaymentMethod != null) {
//                    JSONObject paymentMethodJsonObject = new JSONObject();
//                    paymentMethodJsonObject.put("main", order.mainPaymentMethod.id);
//                    paymentMethodJsonObject.put("sub", order.subPaymentMethod.id);
//                    detailsJsonObject.put("payment_method_id", paymentMethodJsonObject);
//                } else {
//                    JSONObject paymentMethodJsonObject = new JSONObject();
//                    paymentMethodJsonObject.put("main", order.mainPaymentMethod.id);
//                    paymentMethodJsonObject.put("sub", 0);
//                    detailsJsonObject.put("payment_method_id", paymentMethodJsonObject);
//                }
//
//                if (order.token != null) {
//                    detailsJsonObject.put("stripeToken", order.token.getId());
//                    Log.d(CommonConstants.APPLICATION_LOG_TAG, order.token.toString());
//                }
//
//                detailsJsonObject.put("country_id", order.countryId);
//
//
//                if (order.promotion != null) {
//                    detailsJsonObject.put("promotion_codes", order.promotion.promotionCode);
//                }
//
//                for (Product product : order.products) {
//                    JSONObject productJsonObject = new JSONObject();
//                    productJsonObject.put("product_id", product.id);
//                    productJsonObject.put("quantity", product.quantity);
//
//                    if (product.packagingWay != null) {
//                        productJsonObject.put("packaging_id", product.packagingWay.id);
//                    }
//
//                    if (product.giftCard != null) {
//                        productJsonObject.put("card_id", product.giftCard.id);
//
//                        if (product.giftCard.content != null) {
//                            productJsonObject.put("content", product.giftCard.content);
//                        }
//                    }
//
//                    productsJsonArray.put(productJsonObject);
//                }
//
//                detailsJsonObject.put("products", productsJsonArray);
//
//                dataJsonArray.put(detailsJsonObject);
//
//                mainJsonObject.put("data", dataJsonArray);
//                paramJsonObject.put("data", mainJsonObject.toString());
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        } else if (entity instanceof Remainder) {
//            Remainder remainder = (Remainder) entity;
//            JSONObject mainJsonObject = new JSONObject();
//            JSONArray dataJsonArray = new JSONArray();
//            JSONObject detailsJsonObject = new JSONObject();
//            try {
//                detailsJsonObject.put("id", remainder.id);
//                detailsJsonObject.put("user_id", Singleton.getInstance().currentUser.id);
//                detailsJsonObject.put("order_id", String.valueOf(remainder.orderId));
//                detailsJsonObject.put("r_date", remainder.remainderDate);
//                detailsJsonObject.put("details", remainder.details);
//
//                dataJsonArray.put(detailsJsonObject);
//
//                mainJsonObject.put("data", dataJsonArray);
//                paramJsonObject.put("data", mainJsonObject.toString());
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//
//        Log.d(CommonConstants.APPLICATION_LOG_TAG, paramJsonObject.toString());
//        return paramJsonObject;
//    }

    private void executeJSonObjectRequest(int method, final MainListener listener) {
        mFullUrl = getFullUrl();
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "url: \n" + mFullUrl);
        requestTag = initRequestTag();


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, mFullUrl, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "response:  \n" + response.toString());
                ArrayList<Entity> result = new ArrayList<>();

                Entity entity = parser.parse(response);
                result.add(entity);

                listener.onSuccess(result);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null) {
                    if (error.networkResponse.statusCode == 404) {
                        listener.onSuccess(null);
                    }
                } else {
                    listener.onError(error.toString());
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "error without entity:  \n" + error.toString());
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "inside get headers");
                SharedPreferences settings = mContext.getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                        Context.MODE_PRIVATE);
                String token = settings.getString("token", null);
                if (token != null) {
                    headers.put("token", token);
                    Log.d(CommonConstants.APPLICATION_LOG_TAG, "token: " + headers.get("token"));
                }
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d(CommonConstants.APPLICATION_LOG_TAG, "inside get params");

                return params;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, 5,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueueWrapper.addToRequestQueue(jsonObjectRequest, getRequestTag());


    }

    private void executeJsonArrayRequest(int method, final MainListener listener) {
        mFullUrl = getFullUrl();
        Log.d("MyApp", "url: " + mFullUrl);
        requestTag = initRequestTag();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(method, mFullUrl, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                ArrayList<Entity> result = new ArrayList<>();

                for (int i = 0; i < response.length(); i++) {
                    Entity entity = null;
                    try {
                        entity = parser.parse(response.getJSONObject(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    result.add(entity);
                }

                listener.onSuccess(result);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null) {
                    if (error.networkResponse.statusCode == 404) {
                        listener.onSuccess(null);
                    }
                } else {
                    listener.onError(error.toString());
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }

        };
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueueWrapper.addToRequestQueue(jsonArrayRequest, getRequestTag());
    }

    abstract protected void initParser();

    protected enum JsonRequestType {JSON_ARRAY, JSON_OBJECT}

}
