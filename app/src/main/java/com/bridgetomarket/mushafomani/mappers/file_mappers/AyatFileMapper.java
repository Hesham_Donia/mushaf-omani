package com.bridgetomarket.mushafomani.mappers.file_mappers;

import android.content.Context;

import com.bridgetomarket.mushafomani.database.Aya;
import com.bridgetomarket.mushafomani.database.Sura;
import com.bridgetomarket.mushafomani.entities.Entity;
import com.bridgetomarket.mushafomani.interfaces.MainListener;
import com.bridgetomarket.mushafomani.interfaces.Parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by bridgetomarket on 8/1/16.
 */
public class AyatFileMapper extends AbstractFileMapper {

    public AyatFileMapper(Context context) {
        super(context);
    }

    @Override
    protected void initParser() {
        parser = new Parser<JSONObject>() {
            @Override
            public Entity parse(JSONObject suraJsonObject) {

                Aya aya;
                Sura sura = new Sura();
                try {
                    if (suraJsonObject.has("index")) {
                        sura.setIndex(suraJsonObject.getInt("index"));
                    }

                    if (suraJsonObject.has("name")) {
                        sura.setName(suraJsonObject.getString("name"));
                    }

                    if (suraJsonObject.has("aya")) {
                        JSONArray ayatJsonArray = suraJsonObject.getJSONArray("aya");
                        for (int i = 0; i < ayatJsonArray.length(); i++) {
                            aya = new Aya();
                            JSONObject ayaJsonObject = ayatJsonArray.getJSONObject(i);
                            if (ayaJsonObject.has("index")) {
                                aya.setIndex(ayaJsonObject.getInt("index"));
                            }

                            if (ayaJsonObject.has("text")) {
                                aya.setText(ayaJsonObject.getString("text"));
                            }

                            sura.ayat.add(aya);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return sura;
            }
        };
    }

    @Override
    public void getDataFromFile(String fileName, MainListener<ArrayList<? extends Entity>> listener) {
        String dataFromFile = getDataFromFileAsString(fileName);
        startParsingData(dataFromFile, listener);
    }
}
