package com.bridgetomarket.mushafomani.mappers.file_mappers;

import android.content.Context;

import com.bridgetomarket.mushafomani.database.Aya;
import com.bridgetomarket.mushafomani.database.Sura;
import com.bridgetomarket.mushafomani.entities.Entity;
import com.bridgetomarket.mushafomani.interfaces.FileListener;
import com.bridgetomarket.mushafomani.interfaces.MainListener;
import com.bridgetomarket.mushafomani.interfaces.Parser;
import com.bridgetomarket.mushafomani.mappers.AbstractMapper;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * abstract class for any file mapper
 *
 * @author Hesham Khaled , <h.khaled@yellow.com.eg> on 11/12/15.
 * @package com.yellow.eg.Domain.Mapper
 * @className AbstractFileMapper
 **/

abstract public class AbstractFileMapper extends AbstractMapper implements FileListener {

    protected Parser parser = null;
    protected Context mContext;


    public AbstractFileMapper(Context context) {
        mContext = context;
        initParser();
    }

    protected String getDataFromFileAsString(String fileName) {
        String json;
        try {
            InputStream is = mContext.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    protected void startParsingData(String jsonAsString, MainListener<ArrayList<? extends Entity>> listener) {
        try {

            JSONObject jsonObject = new JSONObject(jsonAsString);
            JSONObject quranJsonObject = jsonObject.getJSONObject("quran");
            JSONArray suraJsonArray = quranJsonObject.getJSONArray("sura");
            ArrayList<Sura> suraArrayList = new ArrayList<>();
            for (int i = 0; i < suraJsonArray.length(); i++) {
                suraArrayList.add((Sura) parser.parse(suraJsonArray.getJSONObject(i)));
            }

            listener.onSuccess(suraArrayList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    abstract protected void initParser();
}
