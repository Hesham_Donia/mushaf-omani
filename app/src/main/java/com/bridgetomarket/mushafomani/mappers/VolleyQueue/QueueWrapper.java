package com.bridgetomarket.mushafomani.mappers.VolleyQueue;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;

/**
 * @author Created by Hesham Khaled , h.khaled@yellow.com.eg on 9/15/15.
 * @package com.yellow.eg.Network.Volley
 * @className QueueWrapper
 * <br/> this class used for interacting with the requests queue
 **/
public class QueueWrapper {

    RequestQueue requestQueue;

    public QueueWrapper(Context context) {
        RequestsQueue.context = context;
        requestQueue = RequestsQueue.getRequestQueue();

    }


    /**
     *
     * @param req the request to be added to the queue
     * @param tag the tag of the request
     */
    public void addToRequestQueue(Request req, String tag) {
        // set the default tag if tag is empty
        requestQueue.cancelAll(tag);
        req.setTag(tag);
        requestQueue.add(req);
    }

    /**
     *
     * @param tag the tag of request that will be canceled
     */

    public void cancelPendingRequests(String tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }

    /**
     * cancel all request in the queue
     */
    public void cancelAllRequests() {

        if (requestQueue != null) {
            requestQueue.cancelAll(new RequestQueue.RequestFilter() {
                @Override
                public boolean apply(Request<?> request) {
                    return true;
                }
            });
        }
    }
}
