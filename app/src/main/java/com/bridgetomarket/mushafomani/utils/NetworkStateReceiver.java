package com.bridgetomarket.mushafomani.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;

import com.bridgetomarket.mushafomani.entities.Singleton;
import com.bridgetomarket.mushafomani.interfaces.ObserverListener;
import com.bridgetomarket.mushafomani.interfaces.SubjectListener;


public class NetworkStateReceiver extends BroadcastReceiver implements SubjectListener {

    Singleton singleton;


    public NetworkStateReceiver() {
        singleton = Singleton.getInstance();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "connected");
            notifyObservers(true);
        } else {
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "not connected");
            notifyObservers(false);
        }
    }

    @Override
    public void register(ObserverListener observer) {
        singleton.observers.add(observer);
    }

    @Override
    public void unregister(ObserverListener observer) {
        int index = singleton.observers.indexOf(observer);
        singleton.observers.remove(index);
    }

    @Override
    public void notifyObservers(boolean internetConnectionExist) {
        for (ObserverListener observer : singleton.observers) {
            observer.update(internetConnectionExist);
        }
    }
}
