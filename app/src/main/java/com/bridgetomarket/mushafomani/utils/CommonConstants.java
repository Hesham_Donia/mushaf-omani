package com.bridgetomarket.mushafomani.utils;

/**
 * Created by hesham on 04/04/16.
 */
public class CommonConstants {

    public static final String APPLICATION_LOG_TAG = "mushafApp";
    public static final String SHARED_PREFERENCES_NAME = "mushaf";
    public static final String BASIC_URL = "http://rosemart.online/demo/public/";
    public static final String JSON_FILE_NAME = "quran";
}
