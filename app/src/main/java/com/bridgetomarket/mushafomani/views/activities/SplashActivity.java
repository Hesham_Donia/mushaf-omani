package com.bridgetomarket.mushafomani.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.bridgetomarket.mushafomani.R;
import com.bridgetomarket.mushafomani.controllers.AyatController;
import com.bridgetomarket.mushafomani.utils.CommonConstants;
import com.bridgetomarket.mushafomani.utils.Language;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    Handler handler;
    boolean isInBackground = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Language.loadLanguage(this);

        SharedPreferences settings = getSharedPreferences(CommonConstants.SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);

        if (!settings.getBoolean("isFirstTime", false)) {

            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("isFirstTime", true);
            editor.commit();

            // call controller here
            AyatController ayatController = new AyatController(this);
            ayatController.request(AyatController.AYAT_FILE_ACTION, null);
        }

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isInBackground) {
                    SplashActivity.this.finish();
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    SplashActivity.this.startActivity(intent);
                }
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isInBackground = false;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isInBackground) {
                    SplashActivity.this.finish();
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    SplashActivity.this.startActivity(intent);
                }
            }
        }, SPLASH_TIME_OUT - 1500);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isInBackground = true;
    }
}
