package com.bridgetomarket.mushafomani.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RelativeLayout;


import com.bridgetomarket.mushafomani.entities.Singleton;
import com.bridgetomarket.mushafomani.interfaces.ObserverListener;
import com.bridgetomarket.mushafomani.interfaces.ViewsListener;
import com.bridgetomarket.mushafomani.utils.CommonConstants;
import com.bridgetomarket.mushafomani.utils.Language;
import com.bridgetomarket.mushafomani.utils.NetworkConnection;
import com.bridgetomarket.mushafomani.utils.NetworkStateReceiver;

import java.util.ArrayList;

/**
 * Created by hesham on 04/04/16.
 */
abstract public class AbstractActivity extends AppCompatActivity implements ViewsListener, ObserverListener {

    public boolean isInForegroundMode;
    protected Toolbar mToolbar;
    protected DrawerLayout mDrawer;
    protected ActionBarDrawerToggle mToggle;
    protected NavigationView navView;
    protected RelativeLayout mLoadingContainer;
    protected RecyclerView mNavigationViewRecyclerView;
    private NetworkStateReceiver stateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.loadLanguage(this);
        Singleton.getInstance().currentActivity = this;
        Singleton.activityStack.push(this);
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "stack size is: " + Singleton.activityStack.size());
        stateReceiver = new NetworkStateReceiver();
    }

    @Override
    public void update(boolean internetConnectionExist) {
        if (internetConnectionExist) {
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "Connected from activity");
//            hideNoConnectionContainer();
        } else {
//            showNoConnectionContainer();
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "not Connected from activity");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isInForegroundMode = true;
        stateReceiver.register(this);

        if (NetworkConnection.networkConnectivity(this)) {
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "Connected from activity");
//            hideNoConnectionContainer();
        } else {
//            showNoConnectionContainer();
            Log.d(CommonConstants.APPLICATION_LOG_TAG, "not Connected from activity");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isInForegroundMode = false;

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stateReceiver.unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Singleton.activityStack.pop();
        Log.d(CommonConstants.APPLICATION_LOG_TAG, "stack size is: " + Singleton.activityStack.size());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Singleton.getInstance().currentActivity = this;
//        mNavigationViewRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void onBackPressed() {

        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void closeDrawer() {
        mDrawer.closeDrawer(GravityCompat.START);
    }

//    protected void setupNavigationView() {
//        mNavigationViewRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//        ArrayList<NavigationViewItem> navigationViewItems = new ArrayList<>();
//        navigationViewItems.add(new NavigationViewItem(R.drawable.home, getResources().getString(R.string.home)));
//        navigationViewItems.add(new NavigationViewItem(R.drawable.all_products, getResources().getString(R.string.allProducts)));
//        navigationViewItems.add(new NavigationViewItem(R.drawable.new_arrivals, getResources().getString(R.string.newArrival)));
//        navigationViewItems.add(new NavigationViewItem(R.drawable.events, getResources().getString(R.string.events)));
//        navigationViewItems.add(new NavigationViewItem(R.drawable.language, getResources().getString(R.string.language)));
//        navigationViewItems.add(new NavigationViewItem(R.drawable.contact_us, getResources().getString(R.string.contactUs)));
//        navigationViewItems.add(new NavigationViewItem(R.drawable.more, getResources().getString(R.string.more)));
//        navigationViewItems.add(new NavigationViewItem(R.drawable.auth, getResources().getString(R.string.login)));
//        navigationViewItems.add(new NavigationViewItem(R.drawable.auth, getResources().getString(R.string.logout)));
//
//        NavigationViewRecyclerViewAdapter navigationViewRecyclerViewAdapter = new NavigationViewRecyclerViewAdapter(this, navigationViewItems);
//        mNavigationViewRecyclerView.setAdapter(navigationViewRecyclerViewAdapter);
//    }
//
//    public void hideLoadingView() {
//        mLoadingView.setVisibility(View.GONE);
//    }
//
//    public void showNoConnectionContainer() {
//        mNoConnectionBar.setVisibility(View.VISIBLE);
//        mNoConnectionBar.bringToFront();
//    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, 0);
    }
//
//    public void hideNoConnectionContainer() {
//        mNoConnectionBar.setVisibility(View.GONE);
//    }
//
//    public void showMainLayoutContainer() {
//        mMainLayoutContainer.setVisibility(View.VISIBLE);
//    }
//
//    public void hideMainLayoutContainer() {
//        mMainLayoutContainer.setVisibility(View.GONE);
//    }
//
//    public boolean isMainLayoutContainerVisible() {
//        return mMainLayoutContainer.getVisibility() == View.VISIBLE;
//    }


}
