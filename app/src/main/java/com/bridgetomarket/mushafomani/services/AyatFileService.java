package com.bridgetomarket.mushafomani.services;

import com.bridgetomarket.mushafomani.entities.Entity;
import com.bridgetomarket.mushafomani.entities.Filter;
import com.bridgetomarket.mushafomani.interfaces.FileListener;
import com.bridgetomarket.mushafomani.interfaces.MainListener;
import com.bridgetomarket.mushafomani.models.AbstractModel;
import com.bridgetomarket.mushafomani.models.AyatFileModel;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by bridgetomarket on 8/1/16.
 */
public class AyatFileService extends AbstractService implements FileListener {

    /**
     * @param model
     */
    @Inject
    public AyatFileService(AyatFileModel model) {
        super(model);
    }

    @Override
    public void getDataFromFile(String fileName, MainListener<ArrayList<? extends Entity>> listener) {
        ((AyatFileModel) model).getDataFromFile(fileName, listener);
    }

    @Override
    public void find(String id, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void find(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void findAll(Filter filters, MainListener<ArrayList<? extends Entity>> listener) {

    }

    @Override
    public void save(Entity entity, MainListener<ArrayList<? extends Entity>> listener) {

    }
}
