package com.bridgetomarket.mushafomani.services;


import com.bridgetomarket.mushafomani.interfaces.ServiceListener;
import com.bridgetomarket.mushafomani.models.AbstractModel;

/**
 * Created by hesham on 04/04/16.
 */
abstract public class AbstractService implements ServiceListener {

    protected AbstractModel model;

    /**
     *
     * @param model
     */
    public AbstractService(AbstractModel model){
        this.model = model;
    }
}
