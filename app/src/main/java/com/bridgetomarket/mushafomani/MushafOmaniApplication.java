package com.bridgetomarket.mushafomani;

import android.app.Application;

import com.bridgetomarket.mushafomani.dagger.ApplicationModule;
import com.bridgetomarket.mushafomani.dagger.AyatFileModule;

import java.util.Arrays;
import java.util.List;

import dagger.ObjectGraph;

/**
 * Created by bridgetomarket on 8/1/16.
 */
public class MushafOmaniApplication extends Application {

    public static ObjectGraph objectGraph;

    public void onCreate() {
        super.onCreate();
        Object[] modules = getModules().toArray();
        objectGraph = ObjectGraph.create(modules);
        objectGraph.inject(this);
    }

    protected List<Object> getModules() {
        ApplicationModule applicationModule = new ApplicationModule(this);
        return Arrays.<Object>asList(
                applicationModule,
                new AyatFileModule()
        );
    }
}
